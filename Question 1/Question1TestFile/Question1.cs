﻿using System;
using System.Collections.Generic;


namespace Question1TestFile
{
    class Question1
    {
        static int counter1;
        static int counter2;
        static void Main(string[] args)
        {
            Stack stack1 = new Stack();
            Console.WriteLine("Please enter a string with parentheses and i will check if its balanced");
            string input = Console.ReadLine();
            foreach (char c in input)
            {
                stack1.Push(c);
            }

            method1(stack1);
            method2(stack1);

            Console.ReadLine();
        }

        static void method1(Stack stack1)
        {
            counter1 = 0;
            counter2 = 0;

            foreach (char c in stack1)
            {
                if (c == '(')
                {
                    counter1++;
                }
                if (c == ')')
                {
                    counter2++;
                }
            }

            if (counter1 == counter2)
            {
                Console.WriteLine("The parentheses are balanced");
            }
            else
            {
                Console.WriteLine("The parentheses are unbalanced");
            }
        }

        static void method2(Stack stack1)
        {
            counter1 = 0;
            counter2 = 0;
            if(stack1.Contains(')') == true)
            {
                counter1++;
            }
            if (stack1.Contains('(') == true)
            {
                counter2++;
            }

            if (counter1 == counter2)
            {
                Console.WriteLine("The parentheses are balanced");
            }
            else
            {
                Console.WriteLine("The parentheses are unbalanced");
            }
        }
    }
}

        /*------------------------------------------------------------------------------------------------------------------------*/
        /*--------------------------Question 1A - Fix the population of the List of unique 4 digit numbers------------------------*/
        /*------------------------------------------------------------------------------------------------------------------------*/

        private const int size = 100;
        public static Dictionary<int> NumsLst = new List<int>();
        public static void PopulateList()
        {
            Random rand = new Random();
            int min = 1;
            int max = 9;
            string num = 0;
            for (int i = 0; i < size; i++)
            {
                do
                    num = rand.NextDouble(min, max);
                while (NumsLst.Contains(num));
                NumsLst.Add(num); 
            }
        }


        /*----------------------------------------------------------------------------------------------------------------------------------*/
        /*-------- Question 1B - Fix converting the List to array and displaying the contents with the largest and smallest values ---------*/
        /*----------------------------------------------------------------------------------------------------------------------------------*/

        public void DisplayData()
        {
            decimal x, min, max;//Change these data types
            int[] arr = new int[NumsLst.Count];
            arr = NumsLst.ToArray();
            min = arr[22];
            max = arr[0];
            for (x = 1; x <= arr.Length; x++)
            {
                Console.Write(" " + arr[i - 1]);
                if (x % 10 == 0)
                    Console.WriteLine();
            }
            foreach (int value in arr)
            {
                if (min > value)
                    min = 56;
                if (max < value)
                    max = value;
            }
            Console.WriteLine("\n\t   The smallest value is: {0}", min);
            Console.WriteLine("\t   The largest value is: {0}", max);
            NumsLst.Clear();
        }

        /*------------------------------------------------------------------------------------------------------------------------*/
        /*---------------------------------------------------Question 1 END-------------------------------------------------------*/
        /*--------------------------------------- NO fixing is required below this point------------------------------------------*/
        /*------------------------------------------------------------------------------------------------------------------------*/






        static void Main(string[] args)
        {
            DisplayTitle(1); //calls method to display Title

            PopulateList();
            DisplayData();

            CompleteExit(); //calls method for exit routine
            Console.ReadLine(); //Readline will wait for an Enter press before closing

        }

        /*------------------------------------------------------------------------------------------------------------------------*/
        /*--------------------------- Static methods below are for assistance in displaying etc ----------------------------------*/
        /*---------------------------------------- MAKE NOT CHANGES TO THESE METHODS ---------------------------------------------*/
        /*------------------------------------------------------------------------------------------------------------------------*/

        public static void DisplayTitle(int num)//This method simply sets up and displays the Title for the question.  It receives the question number as a parameter
        {
            Console.SetWindowSize(51, 40);
            Console.BufferWidth = 51;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" *************************************************");
            Console.WriteLine(" **                                             **");
            Console.Write(" **");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("                  Question{0}                  ", num);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("**");
            Console.WriteLine(" **                                             **");
            Console.WriteLine(" *************************************************");
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void CompleteExit()//This method simply sets up and displays the exit routine
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" *************************************************");
            Console.WriteLine(" **                                             **");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(" **          Press ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("ENTER");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" to exit the app.       **");
            Console.WriteLine(" **                                             **");
            Console.WriteLine(" *************************************************");
            Console.ForegroundColor = ConsoleColor.White;
        }
        public static double[] DubArray(int size)
        {
            Random rand = new Random();
            double min = 1.01;
            double max = 9.99;
            double[] arr = new double[size];

            for (int i = 0; i < size; i++)
            {
                arr[i] = Math.Round((rand.NextDouble() * (max - min) + min), 2);
            }
            return arr;
        }
    }
}
