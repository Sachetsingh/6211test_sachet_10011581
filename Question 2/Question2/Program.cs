﻿/*------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------Question 2 ------------------------------------------------------*/
/*---------------------------------Learning outcomes 1 & 2 - Data Types and Recurssion------------------------------------*/
/*----------------------------------------------------Marks - Out of 10----------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question2
{
/*------------------------------------------------------------------------------------------------------------------------*/
/*-----------Question 2 - Implement a recursive method to find a factorial using hardcoded and user input numbers---------*/
/*------------------------------------------------------Marks - Out of 10-------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
/*---When the array is passed into the method it finds the center value of the array and checks if the value being
 search for is found or if it’s higher or lower. If it’s found the method returns the the value. If it’s not found in
 that iteration,the method figures out if the value is higher or lower adjusts the values of the upper and lower variables
 accordingly and calls itself to perform the same operation until the value is found----*/
/*------------------------------------------------------------------------------------------------------------------------*/
    class Program
    {
        static void Main(string[] args)
        {
            const int size = 1000;
            int[] arr = new int[size];
            Stopwatch st = new Stopwatch();

            arr = Populate(arr);                        //Populates the array with random numbers
            Display(arr);
            Console.WriteLine("Please enter a number between 1 and 200 to search for: ");
            int value = Convert.ToInt32(Console.ReadLine());

            st.Reset();                                 //Linear Search befor the data is sorted
            st.Start();
            bool check = Linear(value, arr);
            st.Stop();
            Console.WriteLine("Linear Time taken to find the value {0}: {1}", value, st.Elapsed);

            arr = BubbleSort(arr);                      //Sort the array

            st.Reset();                                 
            st.Start();
            int output1 = BinarySearch(value, arr);     //Binary Search
            st.Stop();
            Console.WriteLine("Binary Time taken to find the value {0}: {1}", output1, st.Elapsed);

            st.Reset();
            st.Start();
            int output2 = RecusiveBinary(arr, value, 0, arr.Length - 1);//Recursive Binary Search
            st.Stop();
            Console.WriteLine("Recursive Binary Time taken to find the value {0}: {1}", output2, st.Elapsed);

            Console.ReadLine();
        }

        /*---Linear Search Algorithm---*/
        public static bool Linear(int value, int[] arr)
        {
            for(int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == value)
                    return true;
            }
            return false;
        }


        /*---Binary Search Algorithm---*/
        public static int BinarySearch(int value, int[] arr)
        {
            int upper, lower, mid;
            upper = arr.Length;
            lower = 0;
            while (lower <= upper)
            {
                mid = (upper + lower) / 2;
                if (arr[mid] == value)
                {
                    return mid;
                }
                else if (value < arr[mid])
                {
                    upper = mid - 1;
                }
                else
                {
                    lower = mid + 1;
                }
            }
            return -1;
        }

        /*---Recursive Binary Search Algorithm---*/
        public static int RecusiveBinary(int[] arr, int value, int lower, int upper)
        {
            if (lower > upper)
            {
                return -1;
            }
            else
            {
                int mid;
                mid = (int)(upper + lower) / 2;
                if (value < arr[mid])
                {
                    return RecusiveBinary(arr, value, lower, mid - 1);
                }
                else if(value == arr[mid])
                {
                    return mid;
                }
                else
                {
                    return RecusiveBinary(arr, value, mid + 1, upper);
                }
            }
        }

        /*---Bubble Sort Algorithm---*/
        public static int[] BubbleSort(int[] _arr)
        {

            int temp;                                   //Variable used to temporarily store the array value while it is swapped around.

            for (int i = 0; i < _arr.Length; i++)       //Outter loops goes through all of the objects in the array.
            {
                for (int j = 0; j < _arr.Length - 1; j++)//Inner loop goes thorough and does the swaps.
                {
                    if (_arr[j] > _arr[j + 1])          //Condition checking of the current state of the array.
                    {
                        temp = _arr[j + 1];             //Swap the contents of the two array indicies.
                        _arr[j + 1] = _arr[j];
                        _arr[j] = temp;
                    }
                }
            }
            return _arr;
        }

        /*---Display Array Method---*/
        public static void Display(int[] arr)
        {
            for (int i = 1; i <= arr.Length; i++)
            {
                Console.Write(arr[i-1] + " ");
                if (i % 10 == 0)                        //will only display 10 value per line.
                    Console.WriteLine();
            }
            Console.WriteLine();
        }

        /*---Populate Array Method---*/
        public static int[] Populate(int[] arr)
        {
            int min = 0;                                //Lowset possible number
            int max = 200;                              //Highest possible number
            Random randNum = new Random();
            for (int i = 0; i < arr.Length; i++)
           {
                arr[i] = randNum.Next(min, max);        //Assign the random number to the i indicies in the array
            }
            return arr;                                 //Return the populated array
        }
    }

/*------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------Question 2 END-------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------------------------------*/
}
